# at86rf215-driver: OS-independent driver for the AT86RF215 transceiver
This project provides an OS-independent implementation for
the for the AT86RF215 transceiver.

## Development Guide
The development is performed on the master branch.
For special cases where a team of developers should work an a common feature,
maintainers may add a special branch on the repository.
However, it will be removed at the time it will be merged on the master branch.
All developers should derive the master branch for their feature branches and merge
requests should also issued at this branch.

Before submitting a new merge request, rebase the master branch and
confirm that the automated CI tests have successfully completed.

### Coding style
For the C code, `at86rf215-driver` uses the Linux Kernel coding style.

At the root directory of the project there is the `astyle` options 
file `.astylerc` containing the proper configuration.
Developers can import this configuration to their favorite editor. 
In addition the `hooks/pre-commit` file contains a Git hook, 
that can be used to perform before every commit, code style formatting
with `astyle` and the `.astylerc` parameters.
To enable this hook developers should copy the hook at their `.git/hooks` 
directory. 
Failing to comply with the coding style described by the `.astylerc` 
will result to failure of the automated tests running on our CI services. 
So make sure that you either import on your editor the coding style rules 
or use the `pre-commit` Git hook.


## Website and Contact
For more information about the project and Libre Space Foundation please visit our [site](https://libre.space/)
and our [community forums](https://community.libre.space).

## License

![Libre Space Foundation](docs/assets/LSF_HD_Horizontal_Color1-300x66.png) 
&copy; 2016-2019 [Libre Space Foundation](https://libre.space).

Licensed under the [GPLv3](LICENSE).
